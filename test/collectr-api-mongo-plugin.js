/*global describe:true, it:true, module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */
/*jshint expr: true*/

'use strict';

var should = require('should')
  , rewire = require('rewire')
  , path = require('path')
  , mongooseMock = require(path.join(__dirname, 'mocks', 'mongodb'))
  , mongooseErrMock = require(path.join(__dirname, 'mocks', 'mongodberr'));

function createCollectrAPIMongoPlugin( envConn, mongoose, options ) {
	var MongoPlugin = rewire(path.join(__dirname, '..', 'lib', 'index.js'));
	MongoPlugin.__set__('envConn', envConn);
	MongoPlugin.__set__('mongoose', mongoose );
	if(typeof options !== 'undefined') {
		MongoPlugin.__set__('options', options);
	}
	return new MongoPlugin();
}

describe('collectr-api-mongo-plugin', function() {
	var mongoFakeConn = 'mongodb://fake:fake@fakehost.com:12345/fakedatabase';
	
	describe('#init', function() {
		
		it('Should initialize', function( done ) {
			var plug = createCollectrAPIMongoPlugin(mongoFakeConn, mongooseMock);
			plug.once('INIT_OK', function( server ) {
				done();
			});
			plug.once('INIT_ERR', function( server ) {
				throw new Error('Failed to initialize');
			});
			plug.init({});
		});

		it('Should fail to initialize with no envConn', function( done ) {
			var plug = createCollectrAPIMongoPlugin('', mongooseMock);
			plug.once('INIT_OK', function( server ) {
				throw new Error('Plugin initialized');
			});
			plug.once('INIT_ERR', function( server ) {
				done();
			})
			plug.init({});
		});
		it('Should fail to initialize with bad options', function( done ) {
			var plug = createCollectrAPIMongoPlugin(mongoFakeConn, mongooseMock, '{badjson');
			plug.once('INIT_OK', function( server ) {
				throw new Error('Plugin initialized');
			});
			plug.once('INIT_ERR', function( server ) {
				done();
			});
			plug.init({});
		});
		it('Should fail to initialize with bad connection', function( done ) {
			var plug = createCollectrAPIMongoPlugin(mongoFakeConn, mongooseErrMock);
			plug.once('INIT_OK', function( server ) {
				throw new Error('Plugin initialized');
			});
			plug.once('INIT_ERR', function( server ) {
				done();
			});
			plug.init({});
		});
	});

	describe('#attach to server', function() {
		it('Should successfully attach to server', function( done ) {
			var MongoPlugin = rewire(path.join(__dirname, '..', 'lib', 'index.js'));
			mongooseMock.getReturnError = null;
			mongooseMock.getReturnValue = 'test';
			MongoPlugin.__set__('envConn', mongoFakeConn);
			MongoPlugin.__set__('mongoose', mongooseMock);
			var pub = new MongoPlugin();
			var server = require(path.join(__dirname, 'mocks', 'server'));
			pub.init(server);
			server[MongoPlugin.varName()] = pub;

			pub.once('INIT_OK', function( server ) {
				done();
			});
		});
	});
});