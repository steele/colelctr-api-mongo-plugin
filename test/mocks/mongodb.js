var events = require('events') 
  , MongoDBMock = function() {}
  , MongooseMock = function() {};

MongoDBMock.prototype = new events.EventEmitter();

MongooseMock.connect = function( url, options ) {

  this.connection = new MongoDBMock();
  setTimeout(function() 
  {
  	this.connection.emit('open');
  }.bind(this), 0);
}
module.exports = MongooseMock;