'use strict';

var callbackStub = function( path, interventionHandler )
{
  //need to pass req, res, next to interventionHandler
    var req = function(){};
    req.getPath = function(){
        return path;
    };
    
    var res = function(){};
    res.send = function(object){
        
    };
    
    interventionHandler(req, res, {});
};
exports.get = exports.put = exports.post = exports.del = callbackStub;