var events = require('events')
  , MongoDBErrorMock = function() {}
  , MongooseErrorMock = function() {};

MongoDBErrorMock.prototype = new events.EventEmitter();

MongooseErrorMock.connect = function( url, options ) {
  this.connection = new MongoDBErrorMock();
  setTimeout(function() 
  {
  	this.connection.emit('error', new Error('some fake error'));
  }.bind(this), 0);
}
module.exports =  MongooseErrorMock;
