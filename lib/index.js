
/*global module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */

'use strict';


var mongoose = require('mongoose')
  , assert = require('assert')
  , events = require('events')
  , path = require('path')
  , envConn = process.env.MONGO_CONNECTION || ''
  , options = process.env.MONGO_OPTIONS || '{"server":{"socketOptions":{"keepAlive":1}}}';

/**
 * Takes a string with - or _ and removes them capitalizing the first letter of each word
 * @param  {String} s the string to camelize
 * @return {String}   a camelized version of s
 */
function camelize( s ) {
	var result = s.replace(/(\-|_|\s)+([\w\W])?/g, function ( mathc, sep, c ) {
		/* istanbul ignore next */
		return (c ? c.toUpperCase() : '');
	});
	return result;
}

/**
 * CollectrMongoPlugin constructor
 */
var CollectrMongoPlugin = function() {
	this.logger = null;
}

/**
 * Get the name of the variable that will point to the instantiated plugin and attached to the server.
 * Extracts the name of this package and uses a camelized version of that.
 * @return {String} the instantiated plugin's variable name when attached to the server
 */
CollectrMongoPlugin.varName = function() {
	return camelize(require(path.join(__dirname, '..', 'package')).name);
}

/**
 * [prototype description]
 * @type {events}
 */
CollectrMongoPlugin.prototype = new events.EventEmitter();

/**
 * Plugin initialization method called by the server after instantiation.  This function
 * must cause the plugin to either emit INIT_ERR or INIT_OK so the server can start listening
 * @param  {} server [description]
 * @param  {[type]} logger [description]
 * @return {[type]}        [description]
 */
CollectrMongoPlugin.prototype.init = function( server, logger ) {
	this.logger = logger;

	try {
		options = JSON.parse(options);
	} catch(e) {
		this.emit('INIT_ERR', server);
		/* istanbul ignore if */
		if(this.logger) {
			this.logger.debug('CollectrMongoPlugin failed to parse MONGO_OPTIONS');
		}
		return;
	}

	if(!envConn) {
		this.emit('INIT_ERR', server);
		/* istanbul ignore if */
		if(this.logger) {
			this.logger.debug('CollectrMongoPlugin failed to initialize missing MONGO_CONNECTION');
		}
		return;
	}
	mongoose.connect(envConn, options);
	server.mongodb = mongoose.connection;
	server.mongodb.on('error', function(err) {
		this.emit('INIT_ERR', server);
		/* istanbul ignore if */
		if(this.logger) {
			this.logger.debug('CollectrMongoPlugin failed to initialize ' +
				'could not connect to Mongo');
		}
	}.bind(this));
	server.mongodb.once('open', function() {
		this.emit('INIT_OK', server);
		/* istanbul ignore if */
		if(this.logger) {
			this.logger.debug('CollectrMongoPlugin Mongo client connection established');
		}
	}.bind(this));
}

/**
 * [exports description]
 * @type {[type]}
 */
module.exports = CollectrMongoPlugin;
